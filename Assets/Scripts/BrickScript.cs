using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    public int tier;
    public Sprite tier3;
    public Sprite tier2;
    public Sprite tier1;

    public SpriteRenderer brickRenderer;

    void Start()
    {
        brickRenderer = GetComponent<SpriteRenderer>();
    }
}
