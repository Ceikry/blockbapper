using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] availableBricks;
    public List<GameObject> activeBricks;
    public GameObject[] availablePowerups;
    public GameObject[] lifeOrbs;
    public Sprite lifeFull;
    public Sprite lifeEmpty;
    public GameObject yesButton;
    public GameObject noButton;
    public GameObject gameOverBoundary;
    public Text gameOverText;
    public AudioClip breakSound;
    public AudioClip dieSound;
    public Text scoreLabel;
    private int livesLeft = 5;

    private int score = 0;

    void Start()
    {
        for(int i = 0; i < lifeOrbs.Length; ++i)
        {
            lifeOrbs[i].GetComponent<SpriteRenderer>().sprite = lifeFull;
        }
        StartCoroutine(makeMoreBricks());
    }

    public void Score(GameObject brick)
    {
        BrickScript sc = brick.GetComponent<BrickScript>();
        switch(sc.tier)
        {
            case 3:
                score += 25;
                sc.tier--;
                sc.brickRenderer.sprite = sc.tier2;
                break;
            case 2:
                score += 50;
                sc.tier--;
                sc.brickRenderer.sprite = sc.tier1;
                break;
            case 1:
                score += 100;
                activeBricks.Remove(brick);
                RollPowerup(brick.transform.position);
                Destroy(brick);
                break;        
        }
        AudioSource.PlayClipAtPoint(breakSound, brick.transform.position);
        scoreLabel.text = "Score: " + score;
    }

    private void RollPowerup(Vector3 position)
    {
        if(Random.Range(0,100) >= 90)
        {
            var index = Random.Range(0, availablePowerups.Length);
            var powerup = (GameObject) Instantiate(availablePowerups[index]);
            powerup.transform.position = position;
        }
    }

    public void NotifyDeath(GameObject ball)
    {
        livesLeft--;
        if(livesLeft < 0)
        {
            var paddle = GameObject.FindWithTag("Player");
            PaddleScript ps = paddle.GetComponent<PaddleScript>();
            BallScript bs = ball.GetComponent<BallScript>();
            ps.Reset();
            bs.Reset();
            gameOverText.gameObject.SetActive(true);
            yesButton.SetActive(true);
            noButton.SetActive(true);
            gameOverBoundary.SetActive(true);
        }
        else
        {
            lifeOrbs[livesLeft].GetComponent<SpriteRenderer>().sprite = lifeEmpty;
            var paddle = GameObject.FindWithTag("Player");
            PaddleScript ps = paddle.GetComponent<PaddleScript>();
            BallScript bs = ball.GetComponent<BallScript>();
            ps.Reset();
            bs.Reset();
        }

        AudioSource.PlayClipAtPoint(dieSound, ball.transform.position);
    }

    private IEnumerator makeMoreBricks()
    {
        while(true)
        {
            if(activeBricks.Count == 0)
            {
                var ball = GameObject.FindWithTag("Ball");
                var bpos = ball.transform.position;
                var pos = new Vector2(-1.72f, 3.28f);
                for(int i = 0; i < 119; ++i)
                {
                    int randIndex = Random.Range(0, availableBricks.Length);

                    if(System.Math.Abs(bpos.x - pos.x) >= 0.02 && System.Math.Abs(bpos.y - pos.y) >= 0.02)
                    {
                        GameObject brick = (GameObject) Instantiate(availableBricks[randIndex]);
                        brick.transform.position = pos;
                        activeBricks.Add(brick);
                    }
                    
                    pos.x += 0.215f;
                    if(pos.x >= 1.75f)
                    {
                        pos.x = -1.72f;
                        pos.y -= 0.215f;
                    }
                }

                if(score > 0)
                {
                    score += 1000;
                    scoreLabel.text = "Score: " + score;
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
    }
}
