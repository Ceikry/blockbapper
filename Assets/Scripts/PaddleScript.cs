using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PaddleScript : MonoBehaviour
{
    public KeyCode moveRight;
    public KeyCode moveLeft;
    public KeyCode fireBall;
    public Text controlsLabel;
    public float speed = 10.0f;
    public GameObject aimArrow;
    private Rigidbody2D rb2d;
    private float xbound = 1.6f;
    private int paddleState = 0; //0 = aiming + firing, 1 = moving left/right, 2 = restart yes/no

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public void Reset()
    {
        paddleState = 0;
        aimArrow.transform.rotation = Quaternion.Euler(0.0f,0.0f,0.0f);
        aimArrow.transform.position = new Vector3(0, -0.705f, 0);
        aimArrow.SetActive(true);
        transform.position = new Vector3(0, -1, 0);
        rb2d.velocity = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(paddleState == 0)
        {
            var rpos = transform.position;
            var rot = aimArrow.transform.rotation;
            rpos.y += 0.08f;

            if(Input.GetKey(moveRight))
            {
                if(rot.z > -0.70f)
                    aimArrow.transform.RotateAround(rpos, Vector3.forward, -40.0f * Time.deltaTime);
            }
            else if(Input.GetKey(moveLeft))
            {
                if(rot.z < 0.70f)
                    aimArrow.transform.RotateAround(rpos, Vector3.forward, 40.0f * Time.deltaTime);
            }
            else if(Input.GetKey(fireBall))
            {
                GameObject ball = GameObject.FindWithTag("Ball");
                Rigidbody2D brb2d = ball.GetComponent<Rigidbody2D>();
                brb2d.AddForce(new Vector2(rot.z * -100.0f, 70 - Math.Abs(rot.z * 100.0f)));
                aimArrow.SetActive(false);
                controlsLabel.gameObject.SetActive(false);
                paddleState = 1;
            }
        }
        else if (paddleState == 1)
        {
            var vel = rb2d.velocity;

            if(Input.GetKey(moveRight))
            {
                vel.x = speed;
            }
            else if(Input.GetKey(moveLeft))
            {
                vel.x = -speed;
            }
            else
            {
                vel.x = 0;
            }

            var pos = transform.position;
            if(pos.x > xbound)
            {
                pos.x = xbound;
            }
            else if(pos.x < -xbound)
            {
                pos.x = -xbound;
            }

            transform.position = pos;

            rb2d.velocity = vel;
        }
    }
}
