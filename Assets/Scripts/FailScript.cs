using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailScript : MonoBehaviour
{
    public GameObject gc;
    public GameController controlScript;

    void Start()
    {
        controlScript = gc.GetComponent<GameController>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Ball")
        {
            var obj = col.gameObject;
            controlScript.NotifyDeath(obj);
        } 
        else if (col.gameObject.tag == "Powerup")
        {
            Destroy(col.gameObject);
        }
    }
}
