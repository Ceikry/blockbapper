using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BallScript : MonoBehaviour
{
    Rigidbody2D rb2d;
    public GameObject gc;
    public bool fired = false;
    private GameController controlScript;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        controlScript = gc.GetComponent<GameController>();
    }

    void Update()
    {
        rb2d.velocity = 3.5f * (rb2d.velocity.normalized);
    }

    public void Reset()
    {
        rb2d.position = new Vector3(0, -0.92f, 0);
        rb2d.velocity = new Vector3(0,0,0);
        fired = false;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            Vector2 vel;
            vel.x = (rb2d.velocity.x / 2) + (coll.collider.attachedRigidbody.velocity.x / 3);
            vel.y = rb2d.velocity.y;

            rb2d.velocity = vel;
        }
        else if(coll.gameObject.tag == "Brick")
        {
            controlScript.Score(coll.gameObject);
        }
        else if(coll.gameObject.tag == "GameOverYes")
        {
            SceneManager.LoadScene("BrickBreaker");
        }
        else if(coll.gameObject.tag == "GameOverNo")
        {
            Application.Quit();
        }
    }
}
