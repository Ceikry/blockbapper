using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour
{
    public int tier;
    public GameObject gc;
    public GameController controlScript;

    void Start()
    {
        gc = GameObject.FindWithTag("GameController");
        controlScript = gc.GetComponent<GameController>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            var bricks = GameObject.FindGameObjectsWithTag("Brick");
            foreach(var brick in bricks)
            {
                var sc = brick.GetComponent<BrickScript>();
                if(sc.tier == tier)
                {
                    controlScript.Score(brick);
                }
            }
            Destroy(this.gameObject);
        }
    }
}
