This isn't much special, just a Breakout clone I made while I was learning the Unity 2D engine. Feel free to use it for educational purposes, or just to play, or whatever you want, really, as long as you respect the AGPL. 

Features:
* Power-ups 
* Randomly generated "Levels" (chunks of 119 blocks)
* Lives
* Game/Scene restarting
* Sound Effects/Music
* UI
* Score tracking

Screenshots:

![image](/uploads/9156a21e06e94bdddb35312963eeed44/image.png)
![image](/uploads/67355b05f4b1d33d7bdbf5d9459dba59/image.png)
![image](/uploads/62443e906c698b4d345dbd9ea45a48dc/image.png)
